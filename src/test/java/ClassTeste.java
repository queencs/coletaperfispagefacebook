import Model.Estados;
import com.opencsv.CSVReader;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class ClassTeste {
    public static void main(String[] args) {

        Reader reader = null;
        try {
            reader = Files.newBufferedReader(Paths.get(System.getProperty("user.dir")+"\\ColetaPerfisPorEstados.csv"));

        CSVReader csvReader = new CSVReader(reader);

            ArrayList<Estados> Estados_Info = new ArrayList<Estados>();
        // Reading Records One by One in a String array
        String[] nextRecord;
        while ((nextRecord = csvReader.readNext()) != null) {
            Estados e = new Estados();
            System.out.println("Estado : " + nextRecord[0]);
            System.out.println("Sigla : " + nextRecord[1]);
            System.out.println("Percent : " + nextRecord[2]);
            e.Estado = nextRecord[0];
            e.Sigla = nextRecord[1];
            e.Percent = Double.valueOf(nextRecord[2].replace(",","."));
            Estados_Info.add(e);
        }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
